package es.pushthebutton.clipboard;

import java.util.ArrayList;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;

/**
 * Activity to be invoked from the Share button. 
 * Get data passed from other apps and send to server
 * 
 * @author Alvaro Guzmán López <alvaro.guzlop+bitbucket@gmail.com> *
 */
public class Share extends Activity
{
	private Sender send;
	
	public void onCreate (Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
	    // Get intent, action and MIME type
	    Intent intent = getIntent();
	    String action = intent.getAction();
	    String type = intent.getType();
	    
	    send = new Sender();
        send.ctx = getApplicationContext();
	    
	    if (Intent.ACTION_SEND.equals(action) && type != null) {
	        if ("text/plain".equals(type)) {
	            handleSendText(intent); // Handle text being sent
	        } else if (type.startsWith("image/")) {
	            handleSendImage(intent); // Handle single image being sent
	        }
	    } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
	        if (type.startsWith("image/")) {
	            handleSendMultipleImages(intent); // Handle multiple images being sent
	        }
	    } else {
	    	Toast.makeText(getApplicationContext(), "Yo dunno", Toast.LENGTH_LONG).show();
	    }
	    finish();
	}

	protected void handleSendText(Intent intent) {
	    String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
	    if (sharedText != null) {
	    	send.toSend = sharedText;
	    	send.execute();
	    }
	}

	protected void handleSendImage(Intent intent) {
	    Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
	    if (imageUri != null) {
	    	Toast.makeText(getApplicationContext(), "Compartir imagen", Toast.LENGTH_LONG).show();
	    }
	}

	protected void handleSendMultipleImages(Intent intent) {
	    ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
	    if (imageUris != null) {
	    	Toast.makeText(getApplicationContext(), "Compartir muchas imagenes", Toast.LENGTH_LONG).show();
	    }
	}	
}
