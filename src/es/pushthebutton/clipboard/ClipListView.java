package es.pushthebutton.clipboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ClipListView extends AsyncTask<Void, Void, ArrayAdapter<String>>
{
	protected Context ctx;
    protected ProgressDialog pDialog;
    protected ListView list;
    
	protected void onPreExecute() 
	{
        super.onPreExecute(); 
        /*pDialog = new ProgressDialog(ctx);
        pDialog.setMessage("Cargando Lista");
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.show();*/
    }
	
	protected ArrayAdapter<String> doInBackground(Void... params)
	{
		JSONArray json = getJsonList();
		List<String> clips = new ArrayList<String>();
		if (json==null)
		{ //Devolver array vacio
			clips.add("No se pudo obtener los clips");			
		} else {
			try {
				for (int i=0; i<json.length(); i++)
				{
					clips.add(json.getJSONObject(i).getString("data"));
				}
			} catch (JSONException e) {
				clips.add("Error leyendo clips");
				e.printStackTrace();
			}
		}
		return new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, clips);
	}
	
	@Override
    protected void onPostExecute(ArrayAdapter<String> result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        list.setAdapter(result);
        //pDialog.dismiss();
    }
	
	private JSONArray getJsonList()
	{
		HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(Keys.URL_GET_CLIPS);
	    HttpResponse response = null;
	    try {	        
	    	List<BasicNameValuePair> postData = new ArrayList<BasicNameValuePair>(1);
			postData.add(new BasicNameValuePair("tokenid", Clipboard.getTokenID(ctx)));
			
	        httppost.setEntity(new UrlEncodedFormEntity(postData));
	        response = httpclient.execute(httppost);
	        
	        if (response.getStatusLine().getStatusCode()!=200) {
	        	Log.w("Clipboard.Cliplistview", "Error HTTP:"+response.getStatusLine().getStatusCode()+" - "+response.getStatusLine().getReasonPhrase());
	        	return null;
	        }
	        Log.v("Clipboard.Cliplistview", "Success HTTP:"+response.getStatusLine().getStatusCode()+" - "+response.getStatusLine().getReasonPhrase());
	        HttpEntity entity = response.getEntity();
	        // json is UTF-8 by default
	        BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"), 8);
	        StringBuilder sb = new StringBuilder();

	        String line = null;
	        while ((line = reader.readLine()) != null)
	        {
	            sb.append(line + "\n");
	        }
	        return new JSONArray(sb.toString());	        
	    } catch (ClientProtocolException e) {
	        Log.e("ClipListView", "ClientProtocolException: "+e.getMessage());
	        Log.w("Clipboard.Cliplistview", "Error HTTP:"+response.getStatusLine().getStatusCode()+" - "+response.getStatusLine().getReasonPhrase());
	    } catch (IOException e) {
	    	Log.e("ClipListView", "IOException: "+e.getMessage());
		} catch (JSONException e) {
			Log.e("ClipListView", "JSONException: "+e.getMessage());
		}
	    return null;
	}
}
