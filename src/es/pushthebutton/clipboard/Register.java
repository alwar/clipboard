package es.pushthebutton.clipboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Manage the registration ID used by webserver to identify this device
 * @author alvaro
 *
 */
public class Register {
    
	public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    //private GoogleCloudMessaging gcm;
    
    protected static final String TAG = "Clipboard.Register";

    
    //private AtomicInteger msgId = new AtomicInteger();
    protected String regid;
    
	
	public Register(Context ctx, Activity app, boolean force)
	{
		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices(ctx, app)) {        	
            regid = getRegistrationId(ctx);

            if (regid.isEmpty() || force ) {
            	Log.i(TAG, "Setting GCM Device ID");
            	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ctx);
                registerInBackground(ctx, gcm);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
	}
	
	
	/**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices(Context ctx, Activity app) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, app,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                app.finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    protected static void storeRegistrationId(Context ctx, String regId) {
        final SharedPreferences prefs = getGcmPreferences(ctx);
        int appVersion = getAppVersion(ctx);
        Log.i(TAG, "Saving regID="+regId.substring(10, 20)+" on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context ctx) {
        final SharedPreferences prefs = getGcmPreferences(ctx);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(ctx);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground(Context ctx, GoogleCloudMessaging gcm) {
    	RegisterGCM task = new RegisterGCM();
    	task.ctx = ctx;
    	task.gcm = gcm;
    	task.execute();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getGcmPreferences(Context ctx) {
        return ctx.getSharedPreferences(Clipboard.class.getSimpleName(), Context.MODE_PRIVATE);
    }
}

class RegisterGCM extends AsyncTask<Void, Void, String>
{
	protected Context ctx;
	protected GoogleCloudMessaging gcm;	
	private int reg_resolution=0;
	protected static final int OP_FAIL    = 128;
	protected static final int OP_SUCCESS = 64;
	
	protected String doInBackground(Void... params) {
        String msg = "";
        String regid;
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(ctx);
            }
            regid = gcm.register(Keys.SENDER_ID);
            msg = "Device registered, registration ID=" + regid;
            
            reg_resolution = registerDevice(regid);
            Register.storeRegistrationId(ctx, regid);
        } catch (IOException ex) {
            msg = "Error :" + ex.getMessage();
        }
        return msg;
    }

    @Override
    protected void onPostExecute(String msg) {
    	if (reg_resolution==OP_SUCCESS)
    		Toast.makeText(ctx, "Clipboard registered", Toast.LENGTH_SHORT).show();
    	else
    		Toast.makeText(ctx, "Can not register device", Toast.LENGTH_SHORT).show();
    }

	private int registerDevice(String did)
	{
		HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(Keys.URL_REGISTER_DEVICE);
	    
	    try {
	        // Add your data
	        List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("tokenid", Clipboard.getTokenID(ctx)));
	        nameValuePairs.add(new BasicNameValuePair("deviceid", did));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        Log.i("ClipboardGCM", "El ID se ha enviado:"+response.getStatusLine().getReasonPhrase()
	        		+ " - "+response.getStatusLine().toString());	        
	        return OP_SUCCESS;
	    } catch (ClientProtocolException e) {
	    	Log.e("ClipboardGCM", "ClientProtocolException: "+e.getMessage());
	    } catch (IOException e) {
	    	Log.e("ClipboardGCM", "IOException: "+e.getMessage());
	    }	    
		return OP_FAIL;		
	}
}
