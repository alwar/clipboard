package es.pushthebutton.clipboard;

/**
 * Static class with api keys and othe sensitive information to easily ignore by git.
 * @author Alvaro Guzmán López <alvaro.guzlop+bitbucket@gmail.com>
 *
 */
public class Keys {
	protected static final String SENDER_ID = "000";
	
	protected static final String URL_REGISTER_DEVICE = "http://example.com/index.php?r=clipboard/RegisterDevice&tokenid=123456";
	protected static final String URL_SEND_CLIP = "http://example.com/index.php?r=clipboard/push&tokenid=123456";
	protected static final String URL_GET_CLIPS = "http://example.com/index.php?r=clipboard/pull&tokenid=123456";
}
