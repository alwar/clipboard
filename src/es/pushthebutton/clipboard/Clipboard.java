package es.pushthebutton.clipboard;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Main Activity... Show some history of clips exchanged
 * 
 * @author Alvaro Guzmán López <alvaro.guzlop+bitbucket@gmail.com> *
 */
public class Clipboard extends Activity 
{
	protected static final String TAG = "Clipboard.Clipboard";
	protected ArrayAdapter<String> adapter;
	private EditText textclip;
    private ListView history;   
    private ClipListView cargador;
	private Sender send;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_clipboard);
		
		textclip = (EditText) findViewById(R.id.sandbox);
        history = (ListView) findViewById(R.id.history);       
        
        //checkForTokenID();
        
        new Register(getApplicationContext(), this, true);
        cargador = new ClipListView();
        cargador.ctx = getApplicationContext();
        cargador.list = history;
        cargador.execute();
        history.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
			      String clip = (String) history.getItemAtPosition(pos);
				ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		        clipboard.setPrimaryClip(ClipData.newPlainText("Netclip",clip));
			}
        	
		});
	}

	public void enviar(View v)
	{
		Toast.makeText(getApplicationContext(), "Intentando enviar...", Toast.LENGTH_SHORT).show();
		send = new Sender();
		send.ctx = getApplicationContext();
		send.toSend = textclip.getText().toString();
		send.history = (ArrayAdapter<String>) history.getAdapter();
		send.execute();
		textclip.setText("");
		
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        checkForTokenID();
        // Check device for Play Services APK.
        new Register(getApplicationContext(), this, false);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.clipboard, menu);
		return true;
	}
	
	public boolean settings(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.forceDeviceid:
			new Register(getApplicationContext(), this, true);
			return true;
		case R.id.setToken:
			askForTokenID();
			return true;
		default:
			Log.w(TAG, "Item "+item.getTitle()+ " has no asociated action");
		}
		return false;
	}
	
	
	public boolean forceDeviceID(View v)
	{
		
		return true;
	}
	
	public boolean setToken(View v)
	{
		askForTokenID();
		return true;
	}
		
	public static void putNotification(String title, String message, Context ctx)
	{
		NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, new Intent(ctx, Clipboard.class), 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setContentTitle(title) //Change this according to the operation realized
	        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
	        .setAutoCancel(true)
	        .setContentText(message);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(1, mBuilder.build());
	}
	
	protected void checkForTokenID()
	{
		String tid = Clipboard.getTokenID(getApplicationContext());
		Toast.makeText(getApplicationContext(), "Existe: "+tid, Toast.LENGTH_SHORT).show();
		if (tid.isEmpty())
			askForTokenID();
	}
	
	protected void askForTokenID()
	{	
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		final Context ctx = getApplicationContext();
		alert.setTitle("Clave de Acceso");
		alert.setMessage("Escriba la clave de acceso");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);
		input.setText(getTokenID(getApplicationContext()));

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				String value = input.getText().toString();
				Clipboard.setTokenID(ctx, value);
				Log.i(TAG, "Clave: "+value+" guardada");
				((ArrayAdapter<String>) history.getAdapter()).notifyDataSetChanged();
			}
		});

		alert.show();
	}
	
	private static void setTokenID(Context ctx, String value)
	{
		final SharedPreferences settings = ctx.getSharedPreferences(Clipboard.class.getSimpleName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
	    editor.putString("tokenid", value);
	    editor.commit();

	}
	
	protected static String getTokenID(Context ctx)
	{
		final SharedPreferences settings = ctx.getSharedPreferences(Clipboard.class.getSimpleName(), Context.MODE_PRIVATE);
		return settings.getString("tokenid", "");
	}
	
	private void showAds()
	{
/*		// Crear la adView
	    ads = new AdView(this, AdSize.BANNER, "ca-app-pub-4997902497059401/8821898041");
	    
	    // Buscar el LinearLayout suponiendo que se le haya asignado
	    // el atributo android:id="@+id/mainLayout"
	    RelativeLayout layout = (RelativeLayout)findViewById(R.id.mainLayOut);

	    // Añadirle la adView
	    layout.addView(ads);

	    // Iniciar una solicitud genérica para cargarla con un anuncio
	    ads.loadAd(null);
*/
	}
}
