package es.pushthebutton.clipboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class Sender extends AsyncTask<Void, Void, Integer>
{
	protected Context ctx;
	protected String toSend;
	protected ArrayAdapter<String> history;
	
	private static final int SUCCESS = 7;
	private static final int IO_EXCEPTION = 1;
	private static final int CP_EXCEPTION = 2;
	private static final int FAIL = 123;
	
	protected Integer doInBackground(Void... params)
	{
		HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(Keys.URL_SEND_CLIP);

	    try {	        
	    	List<BasicNameValuePair> postData = new ArrayList<BasicNameValuePair>(1);
			postData.add(new BasicNameValuePair("tokenid", Clipboard.getTokenID(ctx)));
			postData.add(new BasicNameValuePair("Objects[data]", toSend));
			postData.add(new BasicNameValuePair("Objects[type]", "1"));
	        httppost.setEntity(new UrlEncodedFormEntity(postData));
	        HttpResponse response = httpclient.execute(httppost);
	        toSend = response.getStatusLine().getReasonPhrase();
	        if (response.getStatusLine().getStatusCode()==200)
	        	return SUCCESS;
	        else {
	        	Log.w("Clipboard.Sender", "Error HTTP:"+response.getStatusLine().getStatusCode()+" - "+response.getStatusLine().getReasonPhrase());
	        	return FAIL;
	        }
	    } catch (ClientProtocolException e) {
	        return CP_EXCEPTION;
	    } catch (IOException e) {
	    	return IO_EXCEPTION;
		}
	}
	
	@Override
    protected void onPostExecute(Integer result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (result==SUCCESS) {
        	//Clipboard.putNotification("Clip enviado", "El Clipse ha enviado correctamente", ctx);
        	Toast.makeText(ctx, "El Clipse ha enviado correctamente["+toSend+"]", Toast.LENGTH_LONG).show();
        	if (history!=null)
        		history.notifyDataSetChanged();
        } else
        	//Clipboard.putNotification("Error", "No se pudo enviar el clip", ctx);
        	Toast.makeText(ctx, "No se pudo enviar el clip", Toast.LENGTH_LONG).show();
    }
}
